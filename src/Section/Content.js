import React, {useContext, useState} from "react";
import {Layout} from 'antd';
// Menu, Breadcrumb
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";


import './Section.css'
import 'antd/dist/antd.css'

import logo from './logo.png'
import SideMenu from "./Menu";
import Login from "../Login/Login";
import Register from "../Register/Register";
import {UserContext} from "../Context/UserContext";
import ListMovie from "../Movie/ListMovie";
import ListGame from "../Game/ListGame";
import Home from "../Home/Home";
import ListMovieTable from "../Movie/ListMovieTable";
import MovieCreate from "../Movie/MovieCreate";
import MovieEdit from "../Movie/MovieEdit";
import GameCreate from "../Game/GameCreate";
import GameEdit from "../Game/GameEdit";


const {Header, Content, Footer, Sider} = Layout;


const MainContent = () => {
    const [collapsed, setCollapsed] = useState(false)

    const onCollapse = collapsed => {
        console.log(collapsed);
        setCollapsed(collapsed);
    };

    const [user] = useContext(UserContext);

    const PrivateRoute = ({user, ...props}) => {
        if (user) {
            return <Route {...props} />;
        } else {
            return <Redirect to="/"/>;
        }
    };

    const LoginRoute = ({user, ...props}) =>
        user ? <Redirect to="/movie"/> : <Route {...props} />;
    const RegisRoute = ({user, ...props}) =>
        user ? <Redirect to="/"/> : <Route {...props} />;
    // const ForgotRoute = ({user, ...props}) =>
    //     user ? <Redirect to="/movie"/> : <Route {...props} />;

    return (
        <Router>
            <Layout style={{minHeight: '100vh'}}>
                <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
                    <img src={logo} className="logo" alt={"logo"}/>
                    <SideMenu/>
                </Sider>
                <Layout className="site-layout">
                    <Header className="site-layout-background" style={{padding: 0}}/>
                    <Content style={{margin: '0 16px'}}>
                        <Switch>
                            <Route exact path="/" user={user} component={Home}/>
                            <Route exact path="/movie" user={user} component={ListMovie}/>
                            <PrivateRoute exact path="/movie-table" user={user} component={ListMovieTable}/>
                            <PrivateRoute exact path="/movie/create" user={user} component={MovieCreate}/>
                            <PrivateRoute exact path="/movie/edit/:id" user={user} component={MovieEdit}/>
                            <PrivateRoute exact path="/game-table" user={user} component={ListGame}/>
                            <PrivateRoute exact path="/game/create" user={user} component={GameCreate}/>
                            <PrivateRoute exact path="/game/edit/:id" user={user} component={GameEdit}/>
                            <LoginRoute exact path="/login" user={user} component={Login}/>
                            <RegisRoute exact path="/register" user={user} component={Register}/>
                            {/*<ForgotRoute exact path="/forgot" user={user} component={Forgot}/>*/}
                        </Switch>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>Ant Design ©2018 Created by Ant UED</Footer>
                </Layout>
            </Layout>
        </Router>

    );
}

export default MainContent