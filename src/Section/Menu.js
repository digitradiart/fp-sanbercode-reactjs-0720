import React, {useContext} from "react";
import {Link} from "react-router-dom";

import {
    ContactsOutlined,
    LoginOutlined,
    LogoutOutlined,
    // FileDoneOutlined,
    DingtalkOutlined,
    HomeOutlined
} from '@ant-design/icons';

import {Menu} from "antd";
import {UserContext} from "../Context/UserContext";

function SideMenu() {
    const [user, setUser] = useContext(UserContext);
    const handleLogout = () => {
        setUser(null)
        localStorage.removeItem("user")
        localStorage.removeItem("regis")
    }

    return (
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
            <Menu.Item key="2" icon={<HomeOutlined />}>
                <Link to="/">Game</Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<ContactsOutlined />}>
                <Link to="/movie">Movie</Link>
            </Menu.Item>
            {user &&
            <Menu.Item key="4" icon={<ContactsOutlined />}>
                <Link to="/movie-table">Movie Editor</Link>
            </Menu.Item> }
            {user === null && <Menu.Item key="1" icon={<LoginOutlined />}>
                <Link to="/login">Login</Link>
            </Menu.Item>}
            {/*{user === null &&*/}
            {/*<Menu.Item key="2" icon={<FileDoneOutlined />}>*/}
            {/*    <Link to="/register">Register</Link>*/}
            {/*</Menu.Item>}*/}
            {user &&
            <Menu.Item key="5" icon={<DingtalkOutlined />}>
                <Link to="/game-table">Game Editor</Link>
            </Menu.Item>}

            {user && <Menu.Item key="6" onClick={handleLogout} icon={<LogoutOutlined />}>
                Logout
            </Menu.Item>
            }
        </Menu>
    )
}

export default SideMenu