import React, {Component} from "react";
import axios from "axios";
import '../Movie/ListMovie.css';

class ReadName extends React.Component {
    render() {
        return <h3 style={{color: "#63558e"}}>{this.props.name}</h3>
    }
}

class ReadSinglePlayer extends React.Component {
    render() {
        return <li>Single : {this.props.singlePlayer === 1 ? "YA" : "TIDAK"} </li>
    }
}

class ReadMultiplayer extends React.Component {
    render() {
        return <li>Multiplayer : {this.props.multiplayer === 1 ? "YA" : "TIDAK"}</li>
    }
}

class ReadGenre extends React.Component {
    render() {
        return <li>Genre Game : {this.props.genre}</li>
    }
}

class ReadPlatform extends React.Component {
    render() {
        return <li>Platform Game : {this.props.platform}</li>
    }
}

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Game: [],
            detailGame: []
        }
    }

    componentDidMount() {
        axios.get(`https://backendexample.sanbersy.com/api/games`)
            .then(res => {
                let Game = res.data.map(el => {
                    return {
                        id: el.id,
                        name: el.name,
                        genre: el.genre,
                        singlePlayer: el.singlePlayer,
                        multiplayer: el.multiplayer,
                        platform: el.platform,
                        image_url: el.image_url
                    }
                })
                this.setState({Game})
            })
    }

    render() {
        return (
            <section className={"wrapper-list-movie"}>
                <h1>Daftar Game Terbaik</h1>
                <div className={"list-game"}>
                    {this.state.Game !== null && this.state.Game.map((daftar, index) => {
                        return (
                            <ul key={index}>
                                <ReadName name={daftar.name}/>
                                <ReadGenre genre={daftar.genre}/>
                                <ReadSinglePlayer singlePlayer={daftar.singlePlayer}/>
                                <ReadMultiplayer multiplayer={daftar.multiplayer}/>
                                <ReadPlatform platform={daftar.platform}/>
                                {daftar.image_url == null ? <p>Tidak ada Gambar</p> :
                                    <div className={"wrapper-image"}>
                                        <img src={daftar.image_url} alt={"img"}/>
                                    </div>
                                }
                            </ul>
                        )
                    })}
                </div>
            </section>
        )
    }
}

export default Home