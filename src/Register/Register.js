import React, {useContext, useState} from "react";
import '../Login/Login.css'
import axios from 'axios';
import {UserContext} from "../Context/UserContext";

function Register() {
    const [inputUsername, setInputUsername] = useState("")
    const [inputPassword, setInputPassword] = useState("")

    const [statusForm, ] = useState("regis")
    const [, setUser] = useContext(UserContext);

    const handleChange = (event) => {
        const {name, value} = event.target

        switch (name) {
            case "inputUsername":
                setInputUsername(value);
                break;
            case "inputPassword":
                setInputPassword(value);
                break;
            default:
                break;
        }
    }

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        let username = inputUsername
        let password = inputPassword

        if (username !== "" && password !== "") {
            if (statusForm === "regis") {
                axios.post("https://backendexample.sanbersy.com/api/users", {
                    username,
                    password
                })
                    .then(response => {
                        if (response.status === 201) {
                            alert("Kamu berhasil register")
                            localStorage.setItem("regis", JSON.stringify({username, password}))
                            axios.post("https://backendexample.sanbersy.com/api/login", {
                                username,
                                password
                            })
                                .then(response => {
                                    console.log(response)
                                    if (response.status === 200) {
                                        if (response.data === "invalid username or password") {
                                            alert(response.data)
                                        } else {
                                            setUser("user")
                                            localStorage.setItem("user", JSON.stringify({username, password}))
                                            console.log("pindah ke menu")
                                        }
                                    }
                                })
                        } else {
                            alert(response.data)
                        }
                    })

            }
        }
    }

    return (
        <div className={"wrapper-login"}>
            <h1>Register</h1>
            <form onSubmit={handleSubmit}>
                <div className={"wrapper-input"}>
                    <label>Username</label>
                    <input type={"text"} onChange={handleChange} name="inputUsername" value={inputUsername}/>
                </div>
                <div className={"wrapper-input"}>
                    <label>Password</label>
                    <input type={"password"} onChange={handleChange} name="inputPassword" value={inputPassword}/>
                </div>
                <div className={"wrapper-button"}>
                    <button className={"btn-login"} type="submit" value="submit">Submit</button>
                </div>
            </form>
        </div>
    )
}

export default Register