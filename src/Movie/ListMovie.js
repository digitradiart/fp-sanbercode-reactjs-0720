import React, {Component} from "react";
import axios from "axios";
import './ListMovie.css';

import 'antd/dist/antd.css';
import {Modal} from 'antd';

class ReadTitle extends React.Component {
    render() {
        return <h3 style={{color: "#63558e"}}>{this.props.title}</h3>
    }
}

class ReadDescription extends React.Component {
    render() {
        return <li>Deskripsi : {this.props.description} </li>
    }
}

class ReadDuration extends React.Component {
    render() {
        return <li>Durasi : {this.props.duration/60} jam</li>
    }
}

class ReadGenre extends React.Component {
    render() {
        return <li>Genre : {this.props.genre}</li>
    }
}

class ReadRating extends React.Component {
    render() {
        return <li>Rating : {this.props.rating}</li>
    }
}

class ListMovie extends Component {
    constructor(props) {
        super(props)
        this.state = {
            movies: [],
            detailMovie: [],
            visible: false
        }
    }

    handleOk = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleDetail = (event) => {
        this.setState({
            visible: true,
        });

        let idMovie = parseInt(event.target.value)
        axios.get(`https://www.backendexample.sanbersy.com/api/movies/${idMovie}`)
            .then(response => {
                console.log(response.data)
                let detailMovie =
                    {
                        title: response.data.title,
                        description: response.data.description,
                        duration: response.data.duration,
                        genre: response.data.genre,
                        image_url: response.data.image_url,
                        rating: response.data.rating,
                        review: response.data.review,
                    }
                this.setState({detailMovie})
            })
    }

    handleDelete = (event) => {
        let idMovie = parseInt(event.target.value)

        let newDaftarMovie = this.state.movies.filter(el => el.id !== idMovie)

        axios.delete(` https://backendexample.sanbersy.com/api/movies/${idMovie}`)
            .then(res => {
                console.log(res)
            })

        this.setState({movies : newDaftarMovie})

    }

    componentDidMount() {
        axios.get(`https://www.backendexample.sanbersy.com/api/movies`)
            .then(res => {
                let movies = res.data.map(el => {
                    return {
                        id: el.id,
                        title: el.title,
                        rating: el.rating,
                        duration: el.duration,
                        genre: el.genre,
                        description: el.description
                    }
                })
                this.setState({movies})
            })
    }

    render() {
        return (
            <section className={"wrapper-list-movie"}>
                <h1>Daftar Film Terbaik</h1>
                <div className={"list-movie"}>
                    {this.state.movies !== null && this.state.movies.map((daftar, index) => {
                        return (
                            <ul key={index}>
                                <ReadTitle title={daftar.title}/>
                                <ReadRating rating={daftar.rating}/>
                                <ReadDuration duration={daftar.duration}/>
                                <ReadGenre genre={daftar.genre}/>
                                <ReadDescription description={daftar.description}/>
                                <li>
                                    <button onClick={this.handleDetail} value={daftar.id}>Detail</button>
                                    {/*<button onClick={this.handleDelete} value={daftar.id} className={"btn-delete"}>Delete</button>*/}
                                </li>
                            </ul>
                        )
                    })}
                </div>
                <Modal
                    title="Detail Movie"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <div>
                        <p>Title : {this.state.detailMovie.title}</p>
                        <p>Rating : {this.state.detailMovie.rating}</p>
                        <p>Durasi : {this.state.detailMovie.duration}</p>
                        <p>Genre : {this.state.detailMovie.genre}</p>
                        <p>Deskripsi : {this.state.detailMovie.description}</p>
                        {this.state.detailMovie.image_url == null ? <p>Tidak ada Gambar</p> :
                            <div className={"wrapper-image"}>
                                <img src={this.state.detailMovie.image_url} alt={"img"}/>
                            </div>
                        }
                    </div>
                </Modal>
            </section>
        )
    }
}

export default ListMovie