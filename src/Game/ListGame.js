import React, {Component} from "react";
import axios from "axios";
import '../Movie/ListMovie.css';

import {Modal, Button, Input, Space, Table} from 'antd';

import Highlighter from "react-highlight-words";
import { SearchOutlined } from '@ant-design/icons';
import {Link} from "react-router-dom";


class ListGame extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Game: [],
            detailGame: [],
            visible: false,
            searchText: '',
            searchedColumn: ''
        }
    }

    handleOk = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };



    handleDetail = (event) => {
        this.setState({
            visible: true,
        });

        let idGame = parseInt(event.target.value)
        axios.get(`https://www.backendexample.sanbersy.com/api/games/${idGame}`)
            .then(response => {
                console.log(response.data)
                let detailGame =
                    {
                        name: response.data.name,
                        genre: response.data.genre,
                        singlePlayer: response.data.singlePlayer,
                        multiplayer: response.data.multiplayer,
                        platform: response.data.platform,
                        image_url: response.data.image_url
                    }
                this.setState({detailGame})
            })
    }

    handleDelete = (event) => {
        let idGame = parseInt(event.target.value)

        let newDaftarGame = this.state.Game.filter(el => el.id !== idGame)

        axios.delete(` https://backendexample.sanbersy.com/api/games/${idGame}`)
            .then(res => {
                console.log(res)
            })

        this.setState({Game: newDaftarGame})

    }

    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
                    </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Reset
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex] ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()) : '',
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select(), 100);
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: '' });
    };

    columns = [
        {
            title: 'No',
            dataIndex: 'key',
            sorter: (a, b) => a.key - b.key,
            sortDirections: ['descend'],
        },
        {
            title: 'Nama',
            dataIndex: 'name',
            sorter: (a, b) => a.name.length - b.name.length,
            sortDirections: ['descend'],
            ...this.getColumnSearchProps('name'),
        },
        {
            title: 'Genre',
            dataIndex: 'genre',
            defaultSortOrder: 'descend',
            sorter: (a, b) => a.genre.length - b.genre.length,
            ...this.getColumnSearchProps('genre'),
        },
        {
            title: 'Single Player',
            dataIndex: 'singlePlayer',
            filters: [
                {
                    text: 'YA',
                    value: 'YA',
                },
                {
                    text: 'TIDAK',
                    value: 'TIDAK',
                },
            ],
            filterMultiple: false,
            onFilter: (value, record) => record.singlePlayer.indexOf(value) === 0,
            sorter: (a, b) => a.singlePlayer.length - b.singlePlayer.length,
            sortDirections: ['descend', 'ascend'],
        },
        {
            title: 'Multi Player',
            dataIndex: 'multiplayer',
            filters: [
                {
                    text: 'YA',
                    value: 'YA',
                },
                {
                    text: 'TIDAK',
                    value: 'TIDAK',
                },
            ],
            onFilter: (value, record) => record.multiplayer.indexOf(value) === 0,
            sorter: (a, b) => a.multiplayer.length - b.multiplayer.length,
            sortDirections: ['descend', 'ascend'],
        },
        {
            title: 'Platform',
            dataIndex: 'platform',
            filters: [
                {
                    text: 'PSP',
                    value: 'PSP',
                },
                {
                    text: 'HP',
                    value: 'HP',
                },
            ],
            onFilter: (value, record) => record.platform.indexOf(value) === 0,
            sorter: (a, b) => a.platform.length - b.platform.length,
            sortDirections: ['descend', 'ascend'],
        },
        {
            title: 'Aksi',
            dataIndex: 'detailButton'
        },
        {
            dataIndex: 'editButton'
        },
        {
            dataIndex: 'deleteButton'
        },

    ];


    onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
    }

    componentDidMount() {
        axios.get(`https://backendexample.sanbersy.com/api/games`)
            .then(res => {
                let Game = res.data.map((el, index) => {
                    return {
                        key: index+1,
                        id: el.id,
                        name: el.name,
                        genre: el.genre,
                        singlePlayer: el.singlePlayer === 1 ? "YA" : "TIDAK",
                        multiplayer: el.multiplayer === 1 ? "YA" : "TIDAK",
                        platform: el.platform,
                        image_url: el.image_url,
                        detailButton : <button onClick={this.handleDetail} value={el.id}>Detail</button>,
                        deleteButton : <button onClick={this.handleDelete} value={el.id} className={"btn-delete"}>Delete</button>,
                        editButton: <button value={el.id}><Link to={"/game/edit/" + el.id}>Edit</Link></button>,

                    }
                })
                this.setState({Game})
            })
    }


    render() {
        return (
            <section className={"wrapper-list-movie"}>
                <h1>Daftar Game Terbaik</h1>
                <div style={{
                    display: "flex",
                    width: "100%",
                    justifyContent: "flex-end"
                }}>
                    <button><Link to="/game/create">Tambah Movie</Link></button>
                </div>
                <Table columns={this.columns} dataSource={this.state.Game} onChange={this.onChange}/>
                <Modal
                    title="Detail Game"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <div>
                        <p>Nama : {this.state.detailGame.name}</p>
                        <p>Genre : {this.state.detailGame.genre}</p>
                        <p>SinglePlayer : {this.state.detailGame.singlePlayer === 1 ? "YA" : "TIDAK"}</p>
                        <p>MultiPlayer : {this.state.detailGame.multiplayer === 1 ? "YA" : "TIDAK"}</p>
                        <p>Platform : {this.state.detailGame.platform}</p>
                        {this.state.detailGame.image_url == null ? <p>Tidak ada Gambar</p> :
                            <div className={"wrapper-image"}>
                                <img src={this.state.detailGame.image_url} alt={"img"}/>
                            </div>
                        }
                    </div>
                </Modal>
            </section>
        )
    }
}

export default ListGame